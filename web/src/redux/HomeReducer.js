

const initialState = {
    fruits: [],
    selectedFruit: {},
    fruitPrices: {},
    clients: [],
}

export default function homeReducer(state=initialState, action) {

    switch (action.type){
        case "SET_FRUITS": {
            state = {...state, fruits: action.payload }
            break;
        }
        case "SET_FRUIT_PRICES": {
            state = {...state, fruitPrices: action.payload }
            break;
        }
        case "SET_CLIENTS": {
            state = {...state, clients: action.payload }
            break;
        }
        case "SET_FRUIT": {
            state = {...state, selectedFruit: action.payload }
            break;
        }
    }

    return state;
}
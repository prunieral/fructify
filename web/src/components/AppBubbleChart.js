import { set } from "d3-collection";
import { scaleOrdinal, schemeCategory10, scaleLinear, scaleLog } from  "d3-scale";
import { format } from "d3-format";
import { extent } from "d3-array";

import React from "react";
import PropTypes from "prop-types";

import { ChartCanvas, Chart } from "react-stockcharts";
import { ScatterSeries, CircleMarker } from "react-stockcharts/lib/series";
import { XAxis, YAxis } from "react-stockcharts/lib/axes";
import {
	CrossHairCursor,
	MouseCoordinateX,
	MouseCoordinateY,
} from "react-stockcharts/lib/coordinates";

import AppLoading from './AppLoading.js';
import { fitWidth } from "react-stockcharts/lib/helper";

class AppBubbleChart extends React.Component {
	render() {
		var { data: unsortedData, type, width, height, ratio } = this.props;

		if (unsortedData == null || unsortedData.length == 0 ) {
		    var emptyStyle = { width : width, height : height};
            return (<div style={emptyStyle}>
                        <AppLoading/>
                    </div>);
        }

        console.log('EEE')
        console.log(width)
        console.log(height)

		var data = unsortedData.slice().sort((a, b) => a.mean_test_accuracy - b.mean_test_accuracy);

		var f = scaleOrdinal(schemeCategory10)
			.domain(set(data.map(d => d.algorithm)));
		var r = scaleLinear()
			.range([2, 20])
			.domain(extent(data, d => d.mean_test_accuracy));


		var fill = d => f(d.algorithm);
		var radius = d => r(d.mean_test_accuracy);

		const margin = { left: 50, right: 50, top: 10, bottom: 30 };
		const gridHeight = height - margin.top - margin.bottom;
		const gridWidth = width - margin.left - margin.right;

		return (
			<ChartCanvas ratio={ratio}
			        width={width}
			        height={height}
					margin={margin}
					type={type}
					seriesName="Wealth & Health of Nations"
					data={data}
					xAccessor={d => d.mean_test_accuracy}
					xScale={scaleLog()}
					>
				<Chart id={1}
				        height={gridHeight}
						yExtents={d => d.std_test_accuracy}
						yMousePointerRectWidth={45}
						padding={{ top: 20, bottom: 20 }}>
					<XAxis axisAt="bottom" orient="bottom"
					    ticks={20}
					    tickStroke="#E0E0E0"
					    stroke="#E0E0E0"
					    opacity={0.5}/>
					<YAxis axisAt="left" orient="left"
					    ticks={5}
					    tickStroke="#E0E0E0"
					    stroke="#E0E0E0"
					    opacity={0.5}/>
					<ScatterSeries yAccessor={d => d.std_test_accuracy} marker={CircleMarker}
						fill={fill}
						markerProps={{ r: radius, fill: fill }}/>

					<MouseCoordinateX snapX={false}
						at="bottom"
						orient="bottom"
						rectWidth={50}
						displayFormat={format(".3f")} />
					<MouseCoordinateY
						at="left"
						orient="left"
						displayFormat={format(".3f")} />
				</Chart>
				<CrossHairCursor snapX={false} />
			</ChartCanvas>

		);
	}
}

AppBubbleChart.propTypes = {
	data: PropTypes.array.isRequired,
	width: PropTypes.number.isRequired,
	height: PropTypes.number.isRequired,
	ratio: PropTypes.number.isRequired,
	type: PropTypes.oneOf(["svg", "hybrid"]).isRequired,
};

AppBubbleChart.defaultProps = {
	type: "svg",
	ratio: 1,
};
AppBubbleChart = fitWidth(AppBubbleChart);

export default AppBubbleChart;
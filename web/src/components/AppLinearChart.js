import React, {Component}  from 'react';
import PropTypes from "prop-types";

import AppLoading from './AppLoading.js';

import { scaleTime } from "d3-scale";
import { utcDay } from "d3-time";
import { format } from "d3-format";
import { timeFormat } from "d3-time-format";

import { ChartCanvas, Chart } from "react-stockcharts";
import { XAxis, YAxis } from "react-stockcharts/lib/axes";
import { fitWidth } from "react-stockcharts/lib/helper";
import { last, timeIntervalBarWidth } from "react-stockcharts/lib/utils";
import { discontinuousTimeScaleProvider } from "react-stockcharts/lib/scale";
import { AreaSeries } from "react-stockcharts/lib/series";
import { MouseCoordinateX, MouseCoordinateY } from "react-stockcharts/lib/coordinates";

export default class AppLinearChart extends Component {

    constructor(props) {
        super(props);
        this.state = { data: [] };
    }

    componentDidMount() {
    }

  render() {
        var { data, width, height, style } = this.props;

        if (data == null) {
            return (<div></div>);
        } else {
            var xExtents = [data[0].id, data[data.length-1].id];

            return (
                <ChartCanvas width={width} height={height}
                    margin={{ left: 50, right: 50, top:10, bottom: 30 }}
                    seriesName="MSFT"
                    data={data}
                    type="svg"
                    xAccessor={d => d.id}
                    xScale={scaleTime()}
                    xExtents={xExtents}
                    style={style}>
                    <Chart id={0} yExtents={d => d.value}>
                        <XAxis axisAt="bottom"
                            orient="bottom" ticks={12}
                            tickStroke="#E0E0E0"
                            tickFormat={format(".2")}/>
                        <YAxis axisAt="left"
                            orient="left"
                            tickStroke="#E0E0E0"/>
                        <AreaSeries yAccessor={(d) => d.value}
                            stroke={"#00bcd4"}
						    wickStroke={"#00bcd4"}
						    fill={"#00bcd4"}/>
                    </Chart>
                </ChartCanvas>);
		}
    }
};

AppLinearChart.propTypes = {
	data: PropTypes.array.isRequired,
	width: PropTypes.number.isRequired,
	height: PropTypes.number.isRequired,
};

AppLinearChart.defaultProps = {
	type: "svg",
};
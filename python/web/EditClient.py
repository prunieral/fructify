import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))
from python.db.DB import DB
import json

db = DB()

if len(sys.argv) == 2:
    row = json.loads(sys.argv.pop(1))
    db.modify_client(row)
    print({'info': 'ok'})
else:
    print({'error':'error with parameters'})


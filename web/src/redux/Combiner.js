import { combineReducers } from 'redux';

import globalReducer from './GlobalReducer.js';
import homeReducer from './HomeReducer.js';
import clientReducer from './ClientReducer.js';

export default combineReducers({

    global : globalReducer,
    pagehome : homeReducer,
    pageclient : clientReducer,
})
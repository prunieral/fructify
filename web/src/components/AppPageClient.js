import React, {Component}  from 'react';
import ReactDOM from 'react-dom';
import { TypeChooser } from "react-stockcharts/lib/helper";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { connect } from "react-redux";
import ContainerDimensions from 'react-container-dimensions';
import EnhancedTable from './AppTable.js';
import AppLinearChart from './AppLinearChart.js';
import lang from './lang.js';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import CreateIcon from '@material-ui/icons/Create';
import TimelineIcon from '@material-ui/icons/Timeline';
import DescriptionIcon from '@material-ui/icons/Description';

@connect((store) => {
    return {
        g: store.global,
        store: store.pageclient
    }
})
export default class AppPageClient extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
		axios.get('getClients').then(response => {
            this.props.dispatch({type: "CLIENT_SET_CLIENTS", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
	}

	onRowClick(id) {
	    var selected = this.props.store.clients.filter(c => c['id'] == id)[0];
        this.props.dispatch({type: "CLIENT_SET_SELECTED_CLIENT", payload: selected});
        axios.get('getClientReport?id='+id).then(response => {
            this.props.dispatch({type: "CLIENT_SET_REPORT", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
	}

	onModificationClick(id) {
	    var selected = this.props.store.clients.filter(c => c['id'] == id)[0];
        this.props.dispatch({type: "CLIENT_SET_OPEN_DIALOG", payload: true});
	}

	cancelDialog() {
        this.props.dispatch({type: "CLIENT_SET_OPEN_DIALOG", payload: false});
	}

	validateDialog() {
	    var values = JSON.stringify(this.props.store.selectedClient);
	    axios.get('editClient?values='+values).then(response => {
            this.props.dispatch({type: "CLIENT_SET_OPEN_DIALOG", payload: false});
        }).catch((error) => {
            console.log(error)
        });
	}

	changeTextFieldValue(event, field, value) {
	    var data = {field: field, value: event.target.value};
	    this.props.dispatch({type: "CLIENT_SET_TEXTFIELD_VALUE", payload: data});
	}

	onFruitRowClick(id) {
        this.props.dispatch({type: "SET_HOME_PAGE", payload: 'home'});
        axios.get('getFruitPrices?id='+id).then(response => {
            this.props.dispatch({type: "SET_FRUIT_PRICES", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
        axios.get('getFruitClients?id='+id).then(response => {
            this.props.dispatch({type: "SET_CLIENTS", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
        axios.get('getFruit?id='+id).then(response => {
            this.props.dispatch({type: "SET_FRUIT", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
	}

	openReport(id) {
	    window.open('p/ClientReport?id='+id);
	}

    render() {

        var styles = {
        };

        var r_fruits = this.props.store.report.ranked_fruit ? this.props.store.report.ranked_fruit : [];

		return (
		<div className="page">
		    <div className="title-page">Client</div>
		    <div id="pageConsult-Header">
		        <Grid container
		            alignItems="center"
		            justify="center">
                    <Grid item
		                xs={12} sm={10} md={10} lg={8}>
		                <ContainerDimensions>
                          { ({ width, height }) =>
                              <EnhancedTable
                                name={'Clients'}
                                data={this.props.store.clients}
                                width={width}
                                onClick={this.onRowClick.bind(this)}
                                modificationButton={true}
                                onModificationClick={this.onModificationClick.bind(this)}
                                iconButton={<CreateIcon/>}
                              />
                            }
                        </ContainerDimensions>
                    </Grid>
                    {Object.keys(this.props.store.selectedClient).length > 0 && (
                    <Grid item
		                xs={12} sm={10} md={10} lg={8}
		                style={{marginBottom : 30}}>
		                   <div className="subtitle-page">{lang['ClientInfoTitle'][this.props.g.l]}</div>
		                   <Button
		                        variant="extendedFab"
		                        aria-label="Delete"
		                        onClick={this.openReport.bind(this, this.props.store.selectedClient['id'])}
		                        style={{marginBottom: 20}}>
                                <DescriptionIcon />
                                PDF Report
                           </Button>
                           <Grid container>
                          {Object.keys(this.props.store.selectedClient).map((key, index) => (
                               <Grid item xs={12} sm={12} md={6} lg={6}>
                                    <Grid container>
                                        <Grid item xs={6} sm={6} md={6} lg={6}>
                                            {key.replace(/_/g, " ")} :
                                        </Grid>
                                        <Grid item xs={6} sm={6} md={6} lg={6}>
                                            {this.props.store.selectedClient[key]}
                                        </Grid>
                                    </Grid>
                               </Grid>
                          ))}
                          </Grid>
		                <div className="subtitle-page">{lang['ClientRankingFruit'][this.props.g.l]}</div>
		                <ContainerDimensions>
                          { ({ width, height }) =>
                              <EnhancedTable
                                name={lang['ClientRankingFruit'][this.props.g.l]}
                                data={r_fruits}
                                width={width}
                                modificationButton={true}
                                onModificationClick={this.onFruitRowClick.bind(this)}
                                iconButton={<TimelineIcon/>}
                              />
                            }
                        </ContainerDimensions>
                    </Grid>
                    )}
                </Grid>
            </div>
            <Dialog
                modal={false}
                open={this.props.store.openEditDialog}
                >
                <DialogTitle>{lang['ClientDialogTitle'][this.props.g.l]}</DialogTitle>
                <DialogContent>
                    {Object.keys(this.props.store.selectedClient).map((k, i) => (
                        <FormControl>
                            <TextField
                              disabled={k == 'id'}
                              label={k.replace(/_/g, " ")}
                              value={this.props.store.selectedClient[k]}
                              margin="normal"
                              onChange={this.changeTextFieldValue.bind(this, event, k, this.props.store.selectedClient[k])}
                            />
                        </FormControl>
                    ))}
                </DialogContent>
                <DialogActions>
                    <Button
                        color="secondary"
                        onClick={this.cancelDialog.bind(this)}>
                        {lang['CancelButton'][this.props.g.l]}
                      </Button>
                      <Button
                         color="primary"
                        onClick={this.validateDialog.bind(this)}>
                        OK
                       </Button>
                  </DialogActions>
            </Dialog>
        </div>
		)
	}
}

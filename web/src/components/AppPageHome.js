import React, {Component}  from 'react';
import ReactDOM from 'react-dom';
import { TypeChooser } from "react-stockcharts/lib/helper";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { connect } from "react-redux";
import ContainerDimensions from 'react-container-dimensions';
import EnhancedTable from './AppTable.js';
import AppLinearChart from './AppLinearChart.js';
import axios from 'axios';
import lang from './lang.js';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';

@connect((store) => {
    return {
        g: store.global,
        store : store.pagehome
    }
})
export default class AppPageHome extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
		axios.get('getFruits').then(response => {
            this.props.dispatch({type: "SET_FRUITS", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
	}

	onClickRow(id) {
        axios.get('getFruitPrices?id='+id).then(response => {
            this.props.dispatch({type: "SET_FRUIT_PRICES", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
        axios.get('getFruitClients?id='+id).then(response => {
            this.props.dispatch({type: "SET_CLIENTS", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
        axios.get('getFruit?id='+id).then(response => {
            this.props.dispatch({type: "SET_FRUIT", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
	}

	onChipClick(id) {
	    console.log(id)
        this.props.dispatch({type: "SET_CLIENT_PAGE", payload: 'client'});
        axios.get('getClient?id='+id).then(response => {
            this.props.dispatch({type: "CLIENT_SET_SELECTED_CLIENT", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
        axios.get('getClientReport?id='+id).then(response => {
            this.props.dispatch({type: "CLIENT_SET_REPORT", payload: response.data});
        }).catch((error) => {
            console.log(error)
        });
	}

    render() {

        var styles = {
        };

		return (
		<div className="page">
		    <div className="title-page">{lang['FruitTitle'][this.props.g.l]}</div>
		    <div id="pageConsult-Header">
		        <Grid container
		            alignItems="center"
		            justify="center">
		            <Grid item
		                xs={12} sm={10} md={10} lg={8}>
                        <ContainerDimensions>
                          { ({ width, height }) =>
                              <EnhancedTable
                                name={lang['FruitTableTitle'][this.props.g.l]}
                                data={this.props.store.fruits}
                                width={width}
                                onClick={this.onClickRow.bind(this)}
                              />
                            }
                        </ContainerDimensions>
                    </Grid>
                    {Object.keys(this.props.store.selectedFruit).length > 0 && (
                    <Grid item xs={12} sm={10} md={10} lg={8}>
		                   <div className="subtitle-page">{lang['FruitInfoTitle'][this.props.g.l]}</div>
		                   <Grid container>
                          {Object.keys(this.props.store.selectedFruit).map((key, index) => (
                                <Grid item xs={12} sm={12} md={6} lg={6}>
                                    <Grid container>
                                        <Grid item xs={6} sm={6} md={6} lg={6}>
                                            {key.replace(/_/g, " ")} :
                                        </Grid>
                                        <Grid item xs={6} sm={6} md={6} lg={6}>
                                            {this.props.store.selectedFruit[key]}
                                        </Grid>
                                    </Grid>
                                </Grid>
                          ))}
                          </Grid>
                    </Grid>
                    )}
                    {Object.keys(this.props.store.selectedFruit).length > 0 && (
                    <Grid item xs={12} sm={10} md={10} lg={8}>
		                   <div className="subtitle-page">{lang['FruitCustomer'][this.props.g.l]}</div>
                          {Object.keys(this.props.store.clients).map((index, value) => (
                                <Chip
                                    avatar={<Avatar>{this.props.store.clients[index].name[0]}</Avatar>}
                                    label={this.props.store.clients[index].name}
                                    onClick={this.onChipClick.bind(this, this.props.store.clients[index]['id'])}
                                  />
                          ))}
                    </Grid>
                    )}
                    {Object.keys(this.props.store.selectedFruit).length > 0 && (
                    <Grid item xs={12} sm={10} md={10} lg={8}>
                        <ContainerDimensions>
                          { ({ width, height }) =>
                              <div>
                              {Object.keys(this.props.store.fruitPrices).map((key, index) => (
                                  <div>
                                      <div className="subtitle-page">{lang['FruitPrix'][this.props.g.l]} {key}</div>
                                      <AppLinearChart
                                        data={this.props.store.fruitPrices[key]}
                                        height={width/2}
                                        width={width}
                                      />
                                  </div>
                              ))}
                              </div>
                          }
                        </ContainerDimensions>
                    </Grid>
                    )}
                </Grid>
            </div>
        </div>
		)
	}
}

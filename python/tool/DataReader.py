import pandas as pd
import os


class DataReader:

    data_path = None

    def __init__(self):
        self.data_path = os.path.join(os.path.dirname(__file__), '../../data/')

    def read_table(self, file, index=None, cols=None):
        df = pd.read_csv(os.path.join(self.data_path, file + ".csv"),
                    index_col=index,
                    usecols=cols,
                    na_values=['NaN'])
        return df

    def write_table(self, df, file):
        df.to_csv(os.path.join(self.data_path, file + ".csv"), sep=',', index_label='id')


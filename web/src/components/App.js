import React from "react";
import { connect } from "react-redux";
import ReactDOM from 'react-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import AppMenu from './AppMenu.js';
import AppPageHome from './AppPageHome.js';
import AppPageClient from './AppPageClient.js';
import axios from 'axios';
import 'react-bootstrap-table';
import Button from '@material-ui/core/Button';

@connect((store) => {
    return {
        global : store.global
    };
})
export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.updateDimensions = this.updateDimensions.bind(this);
    }

    updateDimensions() {
        this.props.dispatch({type:"SET_SIZE",
                            payload:{
                                width: window.innerWidth-50,
                                height: window.innerHeight-100
                            }});
    }
    componentWillMount() {
        this.updateDimensions();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    onChangeLanguage(language) {
        this.props.dispatch({type:"SET_LANGUAGE", payload: language});
    }

    render() {
        var theme = createMuiTheme({
          palette: {
            type: "dark",
            primary: {main: "#00bcd4"},
            secondary: {main: "#ff4081"}
          }
        });

        var page = null;
        if (this.props.global != null) {
            if (this.props.global.activePage == 'client') {
              page = <AppPageClient />;
            } else {
              page = <AppPageHome />;
            }
        } else {
            page = <AppPageHome />;
        }

        return(
            <div>
                <div id="menu">
                    <MuiThemeProvider theme={theme}>
                        <AppMenu />
                    </MuiThemeProvider>
                </div>
                <div id="language">
                    <MuiThemeProvider theme={theme}>
                        <Button color="primary" onClick={this.onChangeLanguage.bind(this, 0)}>
                          EN
                        </Button>
                        <Button color="primary" onClick={this.onChangeLanguage.bind(this, 1)}>
                          FR
                        </Button>
                    </MuiThemeProvider>
                </div>
                <div id="body">
                    <div id="page-content"
                        width={this.props.global.size.width}
                        height={this.props.global.size.height}>
                        <MuiThemeProvider theme={theme}>
                            {page}
                        </MuiThemeProvider>
                    </div>
                </div>
            </div>
        );
    }

}

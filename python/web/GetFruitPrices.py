import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))
from python.db.DB import DB
import json

db = DB()

def organize_fruit_prices(fruit):
    fruit = {k: v for k, v in fruit.items() if k.startswith('price_month')}
    return list(fruit.values())

if len(sys.argv) == 2:
    data = {}
    fruit = db.get_fruit(sys.argv.pop(1))
    for currency in db.get_currencies():
        if fruit['currency'] == currency:
            data[currency] = organize_fruit_prices(fruit)
        else:
            data[currency] = list(organize_fruit_prices(fruit) * db.get_change(fruit['currency'], currency))
    for k in data.keys():
        data[k] = [{'id': i+1, 'value': v} for i, v in enumerate(data[k])]
    print(json.dumps(data).replace(' ', ''))
else:
    print({'error':'error with parameters'})


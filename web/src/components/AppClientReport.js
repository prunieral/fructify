import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { TypeChooser } from "react-stockcharts/lib/helper";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { connect } from "react-redux";
import ContainerDimensions from 'react-container-dimensions';
import EnhancedTable from './AppTable.js';
import AppLinearChart from './AppLinearChart.js';
import lang from './lang.js';
import axios from 'axios';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import { MuiThemeProvider, createMuiTheme, createGenerateClassName } from '@material-ui/core/styles';
import JssProvider from 'react-jss/lib/JssProvider';


export default class AppClientReport extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
	}

    render() {
        var { data, sheetsRegistry } = this.props;

        var sheetsManager = new Map();
        var generateClassName = createGenerateClassName();

        for(var i = 0, size = data['ranked_fruit'].length; i < size ; i++){
            data['ranked_fruit'][i] = {id : data['ranked_fruit'][i]['id'],
                                        name : data['ranked_fruit'][i]['name'],
                                        score : data['ranked_fruit'][i]['score']}
        }

        var theme = createMuiTheme({
          palette: {
            type: "dark",
            primary: {main: "#00bcd4"},
            secondary: {main: "#ff4081"}
          }
        });

        var styles = {
            body : {
                marginTop: 15,
                marginLeft: 15,
                backgroundColor: '#212121',
            },
            page : {
                backgroundColor: '#212121',
                padding: '20px',
                width: 750,
                height: 960,
                fontFamily: 'Roboto, sans-serif',
                color: 'white'
            },
            title : {
                textAlign: 'left',
                color: 'rgba(0, 0, 0, 0.15)',
                width: '100%',
                height: 20,
                fontSize: 25,
                color: 'white',
                borderBottomStyle: 'solid',
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(0, 0, 0, 0.2)',
                paddingBottom: 10,
                marginBottom: 10,
                fontVariant: 'small-caps'
            },
            subtitle : {
                textAlign: 'left',
                width: '100%',
                height: 20,
                fontSize: 15,
                color: 'white',
                borderBottomStyle: 'solid',
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(0, 0, 0, 0.2)',
                marginTop: 15,
                paddingBottom: 5,
                marginBottom: 5,
                fontVariant: 'small-caps'
            }
        };

		return (
		<JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
            <MuiThemeProvider theme={theme}>
                <html>
                    <body style={styles.body}>
                        <div style={styles.page}>
                            <div style={styles.title}>{lang['ReportTitle'][0]} : {data['client']['name']}</div>
                            <div>
                                <Grid container
                                    alignItems="center"
                                    justify="center"
                                    style={{marginTop:20}}>
                                    <EnhancedTable
                                        name={lang['ClientRankingFruit'][0]}
                                        data={data['ranked_fruit']}
                                        width={750}
                                      />
                                </Grid>
                                <Grid container
                                    alignItems="center"
                                    justify="center"
                                    style={{marginTop:5}}>
                                    {Object.keys(data['prices_fruit_1']).map((key, index) => (
                                          <Grid xs={4}>
                                              <div style={styles.subtitle}>{data['fruit_1']['name']} : {lang['FruitPrix'][0]} {key}</div>
                                              <AppLinearChart
                                                data={data['prices_fruit_1'][key]}
                                                height={200}
                                                width={250}
                                              />
                                          </Grid>
                                    ))}
                                </Grid>
                                <Grid container
                                    alignItems="center"
                                    justify="center"
                                    style={{marginTop:5}}>
                                    {Object.keys(data['prices_fruit_2']).map((key, index) => (
                                          <Grid xs={4}>
                                              <div style={styles.subtitle}>{data['fruit_2']['name']} : {lang['FruitPrix'][0]} {key}</div>
                                              <AppLinearChart
                                                data={data['prices_fruit_2'][key]}
                                                height={200}
                                                width={250}
                                              />
                                          </Grid>
                                    ))}
                                </Grid>
                            </div>
                        </div>
                    </body>
                </html>
            </MuiThemeProvider>
        </JssProvider>
		)
	}
}

AppClientReport.propTypes = {
	data: PropTypes.object.isRequired,
	sheetsRegistry: PropTypes.object.isRequired,
};

AppClientReport.defaultProps = {
};

var lang =
{

'FruitTitle' : ['Fruit', 'Fruit'],
'ClientTitle' : ['Client', 'Client'],


'FruitTableTitle' : ['Fruits', 'Fruits'],
'FruitInfoTitle' : ['Fruit\'s info', 'Information sur le fruit'],
'FruitCustomer' : ['Potential clients', 'Clients potentiels'],
'FruitPrix' : ['Price in ', 'Prix en '],

'ClientDialogTitle' : ['Modify the client', 'Modifier le client'],
'ClientInfoTitle' : ['Client\'s info', 'Information sur le client'],
'ClientRankingFruit' : ['Client\'s preferences', 'Préférences du client'],

'CancelButton' : ['Cancel', 'Annuler'],

'ReportTitle' : ['Client report', 'Rapport client'],
}

export default lang
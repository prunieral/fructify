require('babel-core/register')({ presets: ['es2015', 'react', 'stage-2'] })
var express = require('express');
var app = express();
var path = require('path');
var React = require('react');
var ReactDOMServer = require('react-dom/server');
var fs = require('fs');
var spawn = require('child_process').spawn;
var axios = require('axios');
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var fs = require('fs');
var readline = require('readline');
var convertHTMLToPDF = require("pdf-puppeteer");
var AppClientReport = require('./src/components/AppClientReport.js').default;
var SheetsRegistry = require('jss').SheetsRegistry;

app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, '..', 'data')))
app.use(bodyParser.json());

var html = fs.readFileSync(path.join(__dirname, 'public/index.html'));
var port = 8000

function getPythonCommand() {
	if (process.platform === "win32") {
	    return "py";
	} else {
	    return "python3.6";
	}
}

app.get('/', function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(html);
})

app.get('/getFruits', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/GetFruits.py')];
    var process = spawn(getPythonCommand(), script);
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).send(data);
    });
})

app.get('/getFruitPrices', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/GetFruitPrices.py')];
    var params = [req.query.id]
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).send(data);
    });
})

app.get('/getFruit', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/GetFruit.py')];
    var params = [req.query.id]
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).send(data);
    });
})

app.get('/getFruitClients', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/GetFruitClients.py')];
    var params = [req.query.id]
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).send(data);
    });
})

app.get('/getClients', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/GetClients.py')];
    var params = []
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).send(data);
    });
})

app.get('/getClient', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/GetClient.py')];
    var params = [req.query.id]
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).send(data);
    });
})

app.get('/getClientReport', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/GetClientReport.py')];
    var params = [req.query.id]
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        res.status(200).end(data);
    });
    process.stderr.on('data', function(data) {
        res.status(500).end(data);
    });
})

app.get('/editClient', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    var script = [path.join(__dirname, '../python/web/EditClient.py')];
    var params = [req.query.values]
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        return res.status(200).send(data);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).send(data);
    });
})

function renderFullPage(html, css) {
  return '<!doctype html><html><head><title>Material-UI</title></head><body><div id="root">'+html+'</div><style id="jss-server-side">'+css+'</style></body></html>';
}

app.get('/p/ClientReport', function(req, res) {
    var script = [path.join(__dirname, '../python/web/GetClientReport.py')];
    var params = [req.query.id]
    var process = spawn(getPythonCommand(), script.concat(params));
    var output = [];
    process.stdout.on('data', function(data) {
        var sheetsRegistry = new SheetsRegistry();
        var params = {data : JSON.parse(data.toString('utf8')), sheetsRegistry : sheetsRegistry}
        var element = React.createElement(AppClientReport, params)
        var html = ReactDOMServer.renderToString(element);
        var css = sheetsRegistry.toString()
        var app = renderFullPage(html, css)
        var callback = function (pdf) {
            res.setHeader("Content-Type", "application/pdf");
            res.send(pdf);
        }
        convertHTMLToPDF(app, callback, []);
    });
    process.stderr.on('data', function(data) {
        return res.status(500).send(data);
    });
})

app.listen(port, function() {
    console.log('Node.js web server at port ' + port + ' is running...')
})
import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))
from python.db.DB import DB
import json

db = DB()

def organize_fruit_prices(fruit):
    fruit = {k: v for k, v in fruit.items() if k.startswith('price_month')}
    return list(fruit.values())

if len(sys.argv) == 2:

    data = {}

    client = db.get_client(sys.argv.pop(1))

    fruits = db.get_fruits_out_of_country(client['country'])
    pref_sizes = [client['fruit_size_preference_3'], client['fruit_size_preference_2'], client['fruit_size_preference_1']]
    pref_curre = [client['fruit_currency_preference_3'], client['fruit_currency_preference_2'],
                  client['fruit_currency_preference_1']]
    for f in fruits:
        score = 0
        score += (pref_sizes.index(f['size']) if f['size'] in pref_sizes else 0) * \
                 (1.2 if client['fruit_type_preference_1'] == 'size' else 1)
        score += (pref_curre.index(f['currency']) if f['currency'] in pref_curre else 0) * \
                 (1.2 if client['fruit_type_preference_1'] == 'currency' else 1)
        f['score'] = score
    ordered_fruits = sorted(fruits, key=lambda k: k['score'], reverse=True)

    for i, f in enumerate(ordered_fruits[0:2]):
        prices = {}
        fruit = db.get_fruit(f['id'])
        for currency in db.get_currencies():
            if fruit['currency'] == currency:
                prices[currency] = organize_fruit_prices(fruit)
            else:
                prices[currency] = list(organize_fruit_prices(fruit) * db.get_change(fruit['currency'], currency))
        for k in prices.keys():
            prices[k] = [{'id': i + 1, 'value': v} for i, v in enumerate(prices[k])]

        data['prices_fruit_' + str(i + 1)] = prices
        data['fruit_' + str(i + 1)] = f

    data['ranked_fruit'] = ordered_fruits
    data['client'] = client

    print(json.dumps(data).replace(' ', ''))
else:
    print({'error':'error with parameters'})


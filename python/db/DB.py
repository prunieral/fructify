import sys
import os
from python.exception.DatabaseException import DatabaseException
#sys.path.append('..')
from python.tool.DataReader import DataReader
import pandas

class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class DB(metaclass=Singleton):

    def __init__(self):
        self.dr = DataReader()

    def get_fruits(self):
        df = self.dr.read_table('fruits',
                                index='id',
                                cols=['id','name','prohibited_country','currency','size','metric'])
        return df.reset_index().to_json(orient='records')

    def get_fruit(self, id=None):
        df = self.dr.read_table('fruits', index='id')
        df = df[df.index == int(id)]
        df.reset_index()
        return df.to_dict(orient='records')[0]

    def get_fruits_out_of_country(self, country):
        df = self.dr.read_table('fruits', index='id')
        df = df[df['prohibited_country'] != country]
        df = df.reset_index()
        return df.to_dict(orient='records')

    def get_change(self, change_from=None, change_to=None):
        df = self.dr.read_table('currencies')
        currency_name = 'price_' + change_to
        df = df[df['currency'] == currency_name]
        df = df.loc[:, df.columns.str.startswith(change_from)]
        return df.as_matrix()[0]

    def get_client(self, id=None):
        df = self.dr.read_table('clients', index='id')
        df = df[df.index == int(id)]
        df = df.reset_index()
        return df.to_dict(orient='records')[0]

    def get_clients(self):
        df = self.dr.read_table('clients', index='id')
        df = df.reset_index()
        return df.to_dict(orient='records')

    def get_clients_out_of_country(self, country):
        df = self.dr.read_table('clients', index='id')
        df = df[df['country'] != country]
        df = df.reset_index()
        return df.to_dict(orient='records')

    def modify_client(self, row):
        newRow = pandas.DataFrame([row])
        newRow = newRow.set_index('id')
        df = self.dr.read_table('clients', index='id')
        df = df.drop(row['id'])
        df = df.append(newRow)[df.columns.tolist()]
        self.dr.write_table(df, 'clients')
        return True

    def get_currencies(self):
        df = self.dr.read_table('currencies', cols=['currency'])
        df['currency'] = df['currency'].str.split('_').str[1]
        return df['currency'].tolist()
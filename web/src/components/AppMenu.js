import React, {Component}  from 'react';
import Paper from '@material-ui/core/Paper'
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import { stack as Menu } from 'react-burger-menu';
import Icon from '@material-ui/core/Icon';
import { connect } from 'react-redux';
import HomeIcon from '@material-ui/icons/Home';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import AccessibilityNewIcon from '@material-ui/icons/AccessibilityNew';
import GestureIcon from '@material-ui/icons/Gesture';
import NetworkCheckIcon from '@material-ui/icons/NetworkCheck';

@connect((store) => {
    return {
        global : store.global
    }
})
export default class AppMenu extends Component {

    setHomePage() {
        this.props.dispatch({type: "SET_HOME_PAGE"});
    }

    setClientPage() {
        this.props.dispatch({type: "SET_CLIENT_PAGE"});
    }

  render() {

        var styles = {
            shortMenu : {
                position: 'fixed',
                top: 80,
                left: 20,
                width: 30,
                marginBottom: 10,
            },
            menuSpace: {
                width: 30,
                height: 30
            }
        }

        return (
        <div>
            <div style={styles.shortMenu}>
                <a id="home"
                    className={this.props.global.activePage == 'home' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setHomePage.bind(this)}>
                    <ShoppingBasketIcon/>
                </a>
                <a id="client"
                    className={this.props.global.activePage == 'client' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setClientPage.bind(this)}>
                    <AccessibilityNewIcon/>
                </a>
                <div style={styles.menuSpace}/>
            </div>
            <Menu width={ 400 } left outerContainerId={ "app" }>
                <a id="home"
                    className={this.props.global.activePage == 'home' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setHomePage.bind(this)}>
                    <ShoppingBasketIcon/>
                    <span className="menu-title">Fruit</span>
                </a>
                <a id="home"
                    className={this.props.global.activePage == 'client' ? 'menu-item-selected' : 'menu-item'}
                    onClick={this.setClientPage.bind(this)}>
                    <AccessibilityNewIcon/>
                    <span className="menu-title">Client</span>
                </a>
                <br/>
            </Menu>
        </div>);
    }
}
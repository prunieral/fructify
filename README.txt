
Here is the PoC of the project.

I implemented those following features :
    - 1.a
    - 1.b
    - 2.a
    - 2.b
    - 2.c.i
    - 2.c.iii
    - 4


#####################
# SETUP STEPS       #
#####################

1. INSTALL PYTHON 3.6 (ON THE WEB)

2. INSTALL PYTHON MODULES

- Open a command prompt
- Go to the "python" directory of this project
- Execute : "python3.6 -m pip install -r requirements.txt"

3. INSTALL NODE.JS 5.6.0 (ON THE WEB)

4. INSTALL NODE MODULES

- Open a command prompt
- Go to the "web" directory of this project
- Execute : "npm install"
- Execute : "npm start"

5. START THE SERVER

- Open a command prompt
- Go to the "web" directory of this project
- Execute : "grunt"

6. GO ON THE APP

- Open Browser
- Open : "localhost:8000"

#####################


Available if needed !
HAVE FUN ! :)

Alexis
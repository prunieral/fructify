
const initialState = {
    activePage : "home",
    size: {},
    l: 0
};

export default function globalReducer(state=initialState, action) {

    switch (action.type){
        case "SET_SIZE": {
            state = {...state, size : action.payload }
            break;
        }
        case "SET_HOME_PAGE": {
            state = {...state, activePage : 'home' }
            break;
        }
        case "SET_CLIENT_PAGE": {
            state = {...state, activePage : 'client' }
            break;
        }
        case "SET_LANGUAGE": {
            state = {...state, l : action.payload }
            break;
        }
    }

    return state;
}
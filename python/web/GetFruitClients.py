import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))
from python.db.DB import DB
import json

db = DB()

if len(sys.argv) == 2:
    fruit = db.get_fruit(sys.argv.pop(1))
    clients = db.get_clients_out_of_country(fruit['prohibited_country'])
    print(json.dumps(clients).replace(' ', ''))
else:
    print({'error':'error with parameters'})


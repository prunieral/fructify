

const initialState = {
    clients: [],
    openEditDialog: false,
    selectedClient: {},
    report: {}
}

export default function clientReducer(state=initialState, action) {

    switch (action.type){
        case "CLIENT_SET_CLIENTS": {
            state = {...state, clients: action.payload }
            break;
        }
        case "CLIENT_SET_OPEN_DIALOG": {
            state = {...state, openEditDialog: action.payload }
            break;
        }
        case "CLIENT_SET_SELECTED_CLIENT": {
            state = {...state, selectedClient: action.payload }
            break;
        }
        case "CLIENT_SET_TEXTFIELD_VALUE": {
            var newValue = state.selectedClient
            newValue[action.payload.field] = action.payload.value
            state = {...state, selectedClient: newValue }
            break;
        }
        case "CLIENT_SET_REPORT": {
            state = {...state, report: action.payload }
            break;
        }
    }

    return state;
}